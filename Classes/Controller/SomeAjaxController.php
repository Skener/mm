<?php


namespace User\Course\Controller;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;

class SomeAjaxController
{


    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function processRequest(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $params = $request->getQueryParams();

        if ($params['action']) {
            switch ($params['action']):
                case 'init':
                    $isLiked = $this->checkUserLikesAction($params);
                    $likesNum = $this->countOnePageLikesAction($params);
                    break;
                case 'show':
                    $likesNum = $this->countAllPagesLikesAction();
                    break;
                case 'liked':
                    $likesNum = $this->createLike($params);
                    $isLiked = $this->checkUserLikesAction($params);
                    break;
            endswitch;

            $newResponse = GeneralUtility::makeInstance(JsonResponse::class);
            $newResponse->setPayload(['isLiked' => $isLiked, 'likesNum' => $likesNum]);
            return $newResponse->withStatus(200);
        }
        return $response->withStatus(404, '[action] parameter not provided.');
    }

    /**
     * @param $params
     * @return bool
     */

    public function checkUserLikesAction($params)
    {
        // Check if FEUser exists
        if ($params['feUserId'] == 0) {
            // Disable likes
            return true;
        } else {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
                'tx_users_news_likes'
            );
            $result = $queryBuilder
                ->select('tx_news_id')
                ->from('tx_users_news_likes')
                ->where($queryBuilder->expr()->eq('tx_fe_user_id', $params['feUserId']))
                ->andWhere($queryBuilder->expr()->eq('tx_news_id', $params['newsArticle']))
                ->execute()
                ->fetch(\PDO::FETCH_COLUMN);

            return (bool)$result;
        }
    }

    /**
     * @param $params
     * @return int
     */
    public function countOnePageLikesAction($params)
    {
//        /** @var QueryBuilder $queryBuilder */
//        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
//            'tx_mm_news_feusers_mm'
//        );
//        $result = $queryBuilder
//            ->select('uid_local')
//            ->from('tx_mm_news_feusers_mm')
//            ->where($queryBuilder->expr()->eq('uid_local', $params['newsArticle']))
//            ->count('uid_foreign')
//            ->groupBy('uid_local')
//            ->execute()
//            ->fetch(\PDO::FETCH_COLUMN);

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
            'tx_news_domain_model_news'
        );
        $result = $queryBuilder
            ->select('n.tx_feuser', 'f.tx_news')
            ->from('tx_news_domain_model_news', 'n')
            ->from('tx_feusers_domain_model_feusers', 'f')
            ->join(
                'f',
                'tx_feusers_domain_model_feusers',
                'b',
                $queryBuilder->expr()->eq(
                    'f.uid',
                    $queryBuilder->quoteIdentifier('n.tx_feuser')
                )
            )
            ->where($queryBuilder->expr()->eq('f.tx_news', $params['newsArticle']))
            ->count('n.tx_feuser')
            ->groupBy('f.tx_news')
            ->execute()
            ->fetch(\PDO::FETCH_COLUMN);


        return (int)$result;
    }

    /**
     * @return mixed
     */
    public function countAllPagesLikesAction()
    {
        $jsonParams = file_get_contents('php://input');
        $newsUid = json_decode($jsonParams);

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
            'tx_users_news_likes'
        );


        //Bad Loop Query

        foreach ($newsUid as $uid) {
            $result = $queryBuilder
                ->select('tx_news_id')
                ->from('tx_users_news_likes')
                ->where($queryBuilder->expr()->eq('tx_news_id', ($uid)))
                ->count('tx_fe_user_id')
                ->groupBy('tx_news_id')
                ->execute()
                ->fetch(\PDO::FETCH_COLUMN);

            $likes [] = (int)$result;
        }


        return $likes;
    }


    /**
     * @param $params
     * @return int
     */
    public function createLike($params)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
            'tx_mm_news_feuser_mm'
        );

        $result = $queryBuilder
            ->insert('tx_mm_news_feuser_mm')
            ->values(
                [
                    'local_uid' => $params['feUserId'],
                    'foreign_uid' => $params['newsArticle']
                ]
            )
            ->execute();
        return $this->countOnePageLikesAction($params);
    }


}
