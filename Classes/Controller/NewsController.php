<?php

namespace User\Course\Controller;


class NewsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * likeRepository
     *
     * @var \User\Course\Domain\Repository\NewsRepository
     * @inject
     */
    protected $newsRepository = null;

    /**
     * action list
     *
     * @param User\News\Domain\Model\News
     * @return void
     */
    public function listAction()
    {

    }

    /**
     * action show
     *
     * @param User\News\Domain\Model\News
     * @return void
     */
    public function showAction(\User\Course\Domain\Model\News $like)
    {
        $this->view->assign('like', $like);
    }

    /**
     * action new
     *
     * @param User\Course\Domain\Model\News
     * @return void
     */
    public function newAction()
    {
    }

    /**
     * action create
     *
     * @param User\News\Domain\Model\News
     * @return void
     */
    public function createAction(\User\Course\Domain\Model\News $newLike)
    {
        $this->addFlashMessage(
            'The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html',
            '',
            \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
        );
        $this->newsRepository->add($newLike);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param User\Course\Domain\Model\News
     * @ignorevalidation $like
     * @return void
     */
    public function editAction(\User\Course\Domain\Model\News $like)
    {
        $this->view->assign('like', $like);
    }

    /**
     * action update
     *
     * @param User\Course\Domain\Model\News
     * @return void
     */
    public function updateAction(\User\Course\Domain\Model\News $like)
    {
        $this->addFlashMessage(
            'The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html',
            '',
            \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
        );
        $this->newsRepository->update($like);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param User\Course\Domain\Model\News
     * @return void
     */
    public function deleteAction(\User\Course\Domain\Model\News $like)
    {
        $this->addFlashMessage(
            'The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html',
            '',
            \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
        );
        $this->newsRepository->remove($like);
        $this->redirect('list');
    }
}
