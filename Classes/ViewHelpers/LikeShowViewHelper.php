<?php


namespace User\Course\ViewHelpers;


use GeorgRinger\News\Domain\Model\News;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\ViewHelperInterface;

class LikeShowViewHelper extends AbstractViewHelper implements ViewHelperInterface
{
    use CompileWithRenderStatic;

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('newsArticle', News::class, 'news item', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $newsUid = $arguments['newsArticle']->getUid();
        $ajaxUrl = '/index.php?eID=mm_ajax&newsArticle=';
        $action = '&action=show';
        $actionUrl = GeneralUtility::locationHeaderUrl(
            $ajaxUrl . $newsUid .  $action
        );

        return $actionUrl;
    }
}
