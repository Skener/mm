<?php


namespace User\Course\Domain\Model;


class FrontUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{

    /**
     * News
     *
     * @var \GeorgRinger\News\Domain\Model\News
     */
    protected $news = null;


    /**
     * Returns the news
     *
     * @return \GeorgRinger\News\Domain\Model\News
     */
    public function getNews(): ?\GeorgRinger\News\Domain\Model\News
    {
        return $this->news;
    }

    /**
     * Sets the news
     *
     * @param \GeorgRinger\News\Domain\Model\News $news
     * @return void
     */
    public function setNews(\GeorgRinger\News\Domain\Model\News $news): void
    {
        $this->news = $news->uid;
    }

}
