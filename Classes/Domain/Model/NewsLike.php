<?php

namespace User\Course\Domain\Model;



/**
 * Course
 */
class Newslike extends \GeorgRinger\News\Domain\Model\News
{


    /**
     * feuser
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $feUser = null;


    /**
     * Returns the feuser
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    public function getFeUser():?\TYPO3\CMS\Extbase\Domain\Model\FrontendUser
    {
        return $this->feUser;
    }

    /**
     * Sets the feuser
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feuser
     * @return void
     */
    public function setFeUser(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feuser):void
    {
        $this->feUser = $feuser->uid;
    }
}
