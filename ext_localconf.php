<?php

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {
        // eID AjaxController=>action
        $GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['mm_ajax'] =
            \User\Course\Controller\SomeAjaxController::class . '::processRequest';
    }
);
