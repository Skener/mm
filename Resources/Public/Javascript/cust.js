((page) => {
  page.onload = async () => {

    // News Article Page
    if (document.getElementsByClassName('news news-single')[0]) {
      let listLikesDiv = document.getElementById('likesCount');
      let url = listLikesDiv.dataset.likeurl;
      try {
        await fetch(url, {
          method: 'POST'
        }).then(function (response) {
          if (response) {
            return response.json();
          }
        }).then(function (jsonLikes) {
          listLikesDiv.innerHTML = `${jsonLikes.likesNum} <span id="heart"> &#x2764</span>`;

          // Enable to Like
          if (jsonLikes.isLiked === false) {
            let heartClick = document.getElementById('heart');
            heartClick.addEventListener('click', function (e) {
              e.stopPropagation();
              var likeUrl = url.replace(/init/, 'liked');
              let res = fetch(likeUrl, {
                method: 'POST'
              }).then(function (res) {
                return res.json();
              }).then(function (result) {
                listLikesDiv.innerHTML = `${result.likesNum} <span id="heart"> &#x2764</span>`;
              });
            });
            let heartNotClick = document.getElementById('heart');
            heartNotClick.removeEventListener('click', () => {
            })
          }
        })
      } catch (e) {
        console.log(e);
      }
    }

    // News List Page
    if (document.getElementsByClassName('news-list-view')[0]) {
      let listLikesDiv = document.getElementsByClassName('likes-list-view')[0];
      let listUrl = listLikesDiv.dataset.likeurl;
      var flatAr = [];
      let showDiv = document.querySelectorAll('.likes-list-view');
      let arUrls = Array.from(showDiv);
      var urs = arUrls.map((ur) => {
        let urss = [];
        urss.push(ur.dataset.likeurl);
        flatAr = flatAr.concat(urss)
        return urss;
      });

      // Get array of News Uids
      var uiDs = getUid(flatAr);

      try {
        let response = await fetch(listUrl, {
          method: 'POST',
          body: JSON.stringify(uiDs)
        });
        let result = await response.json();
        showDiv.forEach(function (div, i) {
          div.innerHTML = `${result.likesNum[i]} <span id="heart"> &#x2764</span>`;
        });
      } catch (e) {
        console.log(e);
      }
    }

    //Get News Uid from array of urls.
    function getUid(ar) {
      var uidStr = [];
      for (var i = 0; i < ar.length; i++) {
        let splUppr = ar[i].split('&');
        for (var j = 0; j < splUppr.length; j++) {
          let splEq = splUppr[j].split('=');
          for (var k = 0; k < splEq.length; k++) {
            uidStr.push(splEq[k]);
            var uid = uidStr.filter(val => {
              return parseInt(val) == val;
            });
          }
        }
      }
      return uid;
    }
  }
})(window);
