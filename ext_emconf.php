<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Many-to-Many RElation',
    'description' => '',
    'category' => 'extension',
    'author' => 'Andriy Tserkovnyk',
    'author_email' => 'andriy.tserkovnyk@resultify.com',
    'state' => 'alpha',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
