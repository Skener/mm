
CREATE TABLE tx_news_domain_model_news (
    tx_feuser int(11) DEFAULT '0' NOT NULL
);


CREATE TABLE tx_feusers_domain_model_feusers (
    tx_news int(11) DEFAULT '0' NOT NULL
);


CREATE TABLE tx_mm_news_feusers_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);
